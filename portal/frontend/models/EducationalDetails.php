<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "educational_details".
 *
 * @property integer $edu_det_id
 * @property string $degree
 * @property string $grade
 * @property string $stream
 * @property string $board
 */
class EducationalDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'educational_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['degree', 'grade', 'stream', 'board'], 'required'],
            [['degree'], 'string', 'max' => 10],
            [['grade'], 'string', 'max' => 5],
            [['stream'], 'string', 'max' => 40],
            [['board'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'edu_det_id' => 'Edu Det ID',
            'degree' => 'Degree',
            'grade' => 'Percentage / SGPA / SGPI / Grade',
            'stream' => 'Stream',
            'board' => 'Board',
        ];
    }
}
