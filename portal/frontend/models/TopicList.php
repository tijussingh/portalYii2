<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "topic_list".
 *
 * @property integer $topic_id
 * @property string $topic_name
 */
class TopicList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_name'], 'required'],
            [['topic_name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Topic ID',
            'topic_name' => 'Topic Name',
        ];
    }
}
