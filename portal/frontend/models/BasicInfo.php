<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "basic_info".
 *
 * @property integer $basic_info_id
 * @property string $DOB
 * @property string $gender
 * @property string $mobile_isd_code
 * @property integer $mobile_no
 * @property string $corr_address
 * @property string $permanent_address
 * @property string $website
 * @property string $hobbies
 * @property string $marital_status
 * @property integer $status
 * @property string $first_login_date
 * @property string $last_profile_update_date
 */
class BasicInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'basic_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DOB', 'gender', 'mobile_isd_code', 'mobile_no', 'corr_address', 'permanent_address', 'marital_status', 'status', 'first_login_date', 'last_profile_update_date'], 'required'],
            [['mobile_no'], 'integer'],
            [['first_login_date', 'last_profile_update_date'], 'safe'],
            [['DOB'], 'string', 'max' => 15],
            [['gender', 'marital_status','status'], 'string', 'max' => 1],
            [['mobile_isd_code'], 'string', 'max' => 4],
            [['corr_address', 'permanent_address'], 'string', 'max' => 100],
            [['website'], 'string', 'max' => 40],
            [['hobbies'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'basic_info_id' => 'Basic Info ID',
            'DOB' => 'Date of Birth',
            'gender' => 'Gender',
            'mobile_isd_code' => 'Mobile ISD Code',
            'mobile_no' => 'Mobile No',
            'corr_address' => 'Correspondance Address',
            'permanent_address' => 'Permanent Address',
            'website' => 'Website',
            'hobbies' => 'Hobbies',
            'marital_status' => 'Marital Status',
            'status' => 'Status',
            'first_login_date' => 'First Login Date',
            'last_profile_update_date' => 'Last Profile Update Date',
        ];
    }
}
