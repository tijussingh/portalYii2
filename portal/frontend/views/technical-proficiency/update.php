<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TechnicalProficiency */

$this->title = 'Update Technical Proficiency: ' . ' ' . $model->tech_prof_id;
$this->params['breadcrumbs'][] = ['label' => 'Technical Proficiencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tech_prof_id, 'url' => ['view', 'id' => $model->tech_prof_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="technical-proficiency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
