<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TechnicalProficiencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Technical Proficiencies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-proficiency-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Technical Proficiency', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tech_prof_id',
            'skill',
            'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
