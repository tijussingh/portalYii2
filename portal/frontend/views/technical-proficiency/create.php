<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TechnicalProficiency */

$this->title = 'Technical Proficiency';
$this->params['breadcrumbs'][] = ['label' => 'Technical Proficiencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-proficiency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
