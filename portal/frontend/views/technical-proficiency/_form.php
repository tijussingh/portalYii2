<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TechnicalProficiency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="technical-proficiency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'skill')->textInput(['maxlength' => 20]) ?>

  <?= $form->field($model, 'level')->dropDownList([' '=>'Select','1'=>'Beginner', '2'=>'Intermediate','3'=>'Professional']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
