<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\WorkExperience */

$this->title = 'Update Work Experience: ' . ' ' . $model->work_exp_id;
$this->params['breadcrumbs'][] = ['label' => 'Work Experiences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->work_exp_id, 'url' => ['view', 'id' => $model->work_exp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-experience-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
