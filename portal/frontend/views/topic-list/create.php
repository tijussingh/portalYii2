<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TopicList */

$this->title = 'Create Topic List';
$this->params['breadcrumbs'][] = ['label' => 'Topic Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
