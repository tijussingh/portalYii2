<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TopicListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Topic Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Topic List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'topic_id',
            'topic_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
