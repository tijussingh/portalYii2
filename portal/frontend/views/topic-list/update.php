<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TopicList */

$this->title = 'Update Topic List: ' . ' ' . $model->topic_id;
$this->params['breadcrumbs'][] = ['label' => 'Topic Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->topic_id, 'url' => ['view', 'id' => $model->topic_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="topic-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
