<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EducationalDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Educational Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educational-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Educational Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'edu_det_id',
            'degree',
            'grade',
            'stream',
            'board',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
