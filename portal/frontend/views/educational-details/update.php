<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\EducationalDetails */

$this->title = 'Update Educational Details: ' . ' ' . $model->edu_det_id;
$this->params['breadcrumbs'][] = ['label' => 'Educational Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->edu_det_id, 'url' => ['view', 'id' => $model->edu_det_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="educational-details-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
