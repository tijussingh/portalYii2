<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\EducationalDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educational-details-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'degree')->dropDownList(['N'=>'Select','12'=>'12','DIPLOMA'=>'DIPLOMA','FE'=>'First Year Engineering','SE'=>'Second Year Engineering','TE'=>'Third Year Engineering','BE'=>'Fourth Year Engineering','O'=>'Others']); ?>

    <?= $form->field($model, 'grade')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'stream')->dropDownList(['N'=>'Select','CS'=>'Computer Science','ETX'=>'Electronics','EXTC'=>'Electronics and Telecommunication','IT'=>'Information Technology']); ?>
    
    <?= $form->field($model, 'board')->textInput(['maxlength' => 30]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
