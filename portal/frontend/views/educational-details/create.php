<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\EducationalDetails */

$this->title = 'Educational Details';
$this->params['breadcrumbs'][] = ['label' => 'Educational Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educational-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
