<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\EducationalDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educational-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'edu_det_id') ?>

    <?= $form->field($model, 'degree') ?>

    <?= $form->field($model, 'grade') ?>

    <?= $form->field($model, 'stream') ?>

    <?= $form->field($model, 'board') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
