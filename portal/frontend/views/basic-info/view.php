<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\BasicInfo */

$this->title = $model->basic_info_id;
$this->params['breadcrumbs'][] = ['label' => 'Basic Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basic-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->basic_info_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->basic_info_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'basic_info_id',
            'DOB',
            'gender',
            'mobile_isd_code',
            'mobile_no',
            'corr_address',
            'permanent_address',
            'website',
            'hobbies',
            'marital_status',
            'status',
            'first_login_date',
            'last_profile_update_date',
            'profile_pic',
        ],
    ]) ?>

</div>
