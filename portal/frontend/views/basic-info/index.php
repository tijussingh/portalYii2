<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BasicInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Basic Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basic-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Fill you credentials below', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'basic_info_id',
            'DOB',
            'gender',
            'mobile_isd_code',
            'mobile_no',
            // 'corr_address',
            // 'permanent_address',
            // 'website',
            // 'hobbies',
            // 'marital_status',
            // 'status',
            // 'first_login_date',
            // 'last_profile_update_date',
            // 'profile_pic',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
