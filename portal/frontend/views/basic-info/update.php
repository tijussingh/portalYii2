<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\BasicInfo */

$this->title = 'Update Basic Info: ' . ' ' . $model->basic_info_id;
$this->params['breadcrumbs'][] = ['label' => 'Basic Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->basic_info_id, 'url' => ['view', 'id' => $model->basic_info_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="basic-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
