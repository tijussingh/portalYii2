<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BasicInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="basic-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'basic_info_id') ?>

    <?= $form->field($model, 'DOB') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'mobile_isd_code') ?>

    <?= $form->field($model, 'mobile_no') ?>

    <?php // echo $form->field($model, 'corr_address') ?>

    <?php // echo $form->field($model, 'permanent_address') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'hobbies') ?>

    <?php // echo $form->field($model, 'marital_status') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'first_login_date') ?>

    <?php // echo $form->field($model, 'last_profile_update_date') ?>

    <?php // echo $form->field($model, 'profile_pic') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
