<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class formsyncController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $basicinfo = \frontend\models\BasicInfo::findOne(Yii::$app->user->getId());
       $edudet = \frontend\models\EducationalDetails::findOne(Yii::$app->user->getId());
       $projects= \frontend\models\Projects::findOne(Yii::$app->user->getId());
       $techprof = \frontend\models\TechnicalProficiency::findOne(Yii::$app->user->getId());
       $workexp = \frontend\models\WorkExperience::findOne(Yii::$app->user->getId());
       
        if(is_null($basicinfo))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=basic-info/create',302);
        }
        else if(is_null($edudet))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=educational-details/create',302);
        }
         else if(is_null($projects))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=projects/create',302);
        }
         else if(is_null($techprof))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=technical-proficiency/create',302);
        }
         else if(is_null($workexp))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=work-experience/create',302);
        }
       
        return $this->render('index');
    }

}
